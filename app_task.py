# -*- coding: utf-8 -*-
import pandas as pd 
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import datetime as dt
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains


timeout = 10

def find_element(element_type, search_string):
    import config
    browser = config.browser_task
    if element_type == 'XPATH':
        by_type = By.XPATH
    if element_type == 'ID':
        by_type = By.ID
    return WebDriverWait(browser, timeout).until(
        EC.presence_of_element_located((by_type, search_string))
    )    
    
def add_lawsuit_info(lawsuit,lawsuit_df, lawsuit_i):
    import config
    browser = config.browser_task
    try:
        browser.execute_script("window.open('');")
        browser.switch_to.window(browser.window_handles[-1])
        browser.get('https://pje.trt20.jus.br'+lawsuit)
    except:
        browser.close()
        browser.switch_to.window(browser.window_handles[-1])
        return "Não foi possível continuar a varredura de processos"
    try:
        cod = find_element('XPATH', "//div[contains(@id,'processoIdentificadorDiv')]//span").text
        name = find_element('XPATH', "//div[contains(@id,'partesDiv')]//span").text
    except:
        browser.close()
        browser.switch_to.window(browser.window_handles[-1])
        return 'Não foi possível carregar informações do processo'
    try:
        last_update = find_element('XPATH',"//td[contains(@id,'processoDocumentoGridTabList:0')][3]").text
        last_update_title = find_element('XPATH',"//td[contains(@id,'processoDocumentoGridTabList:0')][4]").text
        last_update_type = find_element('XPATH',"//td[contains(@id,'processoDocumentoGridTabList:0')][5]").text
    except:
        last_update = last_update_title = last_update_type = ''
    try:
        find_element('XPATH', "//td[contains(@id,'tabProcessoAudiencia_lbl')]").click()
    except:
        browser.close()
        browser.switch_to.window(browser.window_handles[-1])
        return 'Não foi possível avançar para a aba de audiência'
    try:
        last_trial_date = find_element('XPATH',"//td[contains(@id,'processoConsultaAudienciaGridList:0')][1]").text
        last_trial_type = find_element('XPATH',"//td[contains(@id,'processoConsultaAudienciaGridList:0')][2]").text
        last_trial_location = find_element('XPATH',"//td[contains(@id,'processoConsultaAudienciaGridList:0')][4]").text
        last_trial_status = find_element('XPATH',"//td[contains(@id,'processoConsultaAudienciaGridList:0')][5]").text
    except:
        last_trial_date = last_trial_type = last_trial_location = last_trial_status = ''
        
    #lawsuit_df.loc[lawsuit_i] = [cod,name,last_update,last_update_title,last_update_type,last_trial_date,last_trial_type,last_trial_location,last_trial_status]
    lawsuit_df.loc[lawsuit_i] = [cod,name,last_update,last_update_title,last_trial_date,last_trial_status]
    browser.close()
    browser.switch_to.window(browser.window_handles[-1])
    return None

def get_lawsuits():
    import config
    browser = config.browser_task
    try:
        link_list = browser.find_elements_by_xpath("//div[contains(@id,'processoTrfInicialAdvogadoList:')]/div/a[contains(@id,'processoTrfInicialAdvogadoList:')]")
        url_list = [link.get_attribute('onclick').split("'")[1] for link in link_list]
    except: 
        time.sleep(timeout)
        try:
            link_list = browser.find_elements_by_xpath("//div[contains(@id,'processoTrfInicialAdvogadoList:')]/div/a[contains(@id,'processoTrfInicialAdvogadoList:')]")
            url_list = [link.get_attribute('onclick').split("'")[1] for link in link_list]
        except:
            return [],"Não foi possível coletar lista de processos da página"
    return url_list,''

def EXIT(error_message,error_handler): # close task
    error_handler.append(error_message)
    raise Exception

def check_all_lawsuits(progress_bar,errors,result_df,result_messages):
    import config
    browser = config.browser_task
    progress_bar.append(0)

    if len(browser.window_handles) != 2:
        EXIT('O login não pode ser confirmado. Por favor, contate o suporte.',errors)
    browser.switch_to.window(browser.window_handles[1])
    if 'quadroaviso' not in browser.current_url.lower():
        EXIT('O login não pode ser confirmado. Por favor, contate o suporte.',errors)

    # start
    before = time.time()
    # handle open tabs
    browser.switch_to.window(browser.window_handles[0])
    browser.close()
    browser.switch_to.window(browser.window_handles[0])
    
    # navigate to search page
    try:
        find_element('XPATH',"//a[contains(text(), 'Iniciar')]").click()
        find_element('XPATH',"//*[contains(text(), 'Acervo geral')]").click()
        find_element('ID',"leftAdvPnl_header_label").click()
        find_element('ID',"consultaProcessoAdvogadoForm:searchButon").click()
    except:
        EXIT('Não foi possível navegar até a página de pesquisa',errors)
    try:
        max_pages = int(find_element('XPATH',"//td[contains(@class,'rich-inslider-right-num')]").text)
        max_len_lawsuit_list = len(get_lawsuits()[0])
    except:
        EXIT('Não foi possível carregar informações da página de pesquisa',errors)        
    columns = [
            'CÓDIGO','NOME','mov_date','MOVIMENTAÇÃO (NOME)',
            'trial_date','AUDIÊNCIA(STATUS)']
    lawsuit_df = pd.DataFrame([],columns=columns)
    
    progress_bar_step = 99/max_len_lawsuit_list

    page = 1
    for page in range(1,max_pages+1):#max_pages+1
        print("DEBUG Página atual:",page)  
        lawsuit_list, error = get_lawsuits()
        if error:
            EXIT(error,errors)
        progress_bar_small_step = progress_bar_step/len(lawsuit_list)
        for i,lawsuit in enumerate(lawsuit_list):
            print('DEBUG lawsuit',i)
            error = add_lawsuit_info(lawsuit,lawsuit_df,lawsuit_df.shape[0])   
            if error:
                EXIT(error,errors)
            progress_bar[0] = round(progress_bar[0] + progress_bar_small_step,2)
        try:
            if page != max_pages:
                find_element('XPATH',"//div[contains(@class,'rich-inslider-inc-horizontal rich-inslider-arrow')]").click()    
                wait_time = 0
                to_wait = .5
                while wait_time < timeout:
                    new_lawsuits, error = get_lawsuits()
                    if error:
                        EXIT(error,errors)
                    print('len',len(set(new_lawsuits).intersection(set(lawsuit_list))))
                    if len(set(new_lawsuits).intersection(set(lawsuit_list))) == 0:
                        print("avançando loop")
                        break
                    print("esperando")
                    time.sleep(timeout)
                    time.sleep(to_wait)
                    wait_time += to_wait
                if wait_time >= timeout:
                    find_element('XPATH',"//div[contains(@class,'rich-inslider-inc-horizontal rich-inslider-arrow')]").click()    
                    wait_time = 0
                    while wait_time < timeout:
                        new_lawsuits, error = get_lawsuits()
                        if error:
                            EXIT(error,errors)
                        print('len',len(set(new_lawsuits).intersection(set(lawsuit_list))))
                        if len(set(new_lawsuits).intersection(set(lawsuit_list))) == 0:
                            print("avançando loop")
                            break
                        print("esperando 2")
                        time.sleep(timeout)
                        time.sleep(to_wait)
                        wait_time += to_wait
                    if wait_time >= timeout:
                        EXIT('Não foi possível avançar para a próxima página da pesquisa',errors)        
        except:
            EXIT('Não foi possível avançar para a próxima página da pesquisa',errors)        

    lawsuit_df['trial_date'] = pd.to_datetime(lawsuit_df['trial_date'],dayfirst=True)
    lawsuit_df['mov_date'] = pd.to_datetime(lawsuit_df['mov_date'],dayfirst=True)

    lawsuit_df['AUDIÊNCIA (DATA)'] = lawsuit_df['trial_date'].dt.strftime("%d/%m/%Y, %H:%M")
    lawsuit_df['MOVIMENTAÇÃO (DATA)'] = lawsuit_df['mov_date'].dt.strftime("%d/%m/%Y, %H:%M")
    lawsuit_df['AUDIÊNCIA (DATA)'] =  lawsuit_df['AUDIÊNCIA (DATA)'].apply(lambda x:'' if x == 'NaT' else x)
    lawsuit_df['MOVIMENTAÇÃO (DATA)'] =  lawsuit_df['MOVIMENTAÇÃO (DATA)'].apply(lambda x:'' if x == 'NaT' else x)
    
    lawsuit_df = lawsuit_df.drop_duplicates(subset='CÓDIGO', keep='first')

    browser.quit()

    result_df.append(lawsuit_df)
    result_messages.append('Processos analisados:{}'.format(lawsuit_df.shape[0]))
    result_messages.append('Tempo decorrido em segundos:{:.1f}'.format(time.time()-before))
    progress_bar[0] = 100