from flask import Flask, render_template, Response, url_for, redirect, request, send_file
import time 
import threading
from app_task import check_all_lawsuits
import threading

url_base = "http://127.0.0.1:5000/"
app = Flask(__name__)
execution_started = False
progress_bar = [0]
result_df = []
errors = []
result_messages = []
temporary_dir = 'temp/'


@app.route("/")
def index():
    global execution_started, errors, result_df
    if errors:
            return redirect(url_for('error'))
    if not (result_df == []):
        return redirect(url_for('results'))
    if execution_started:
        return redirect(url_for('start'))
    return render_template('index.html')

@app.route("/confirm_login/")
def confirm_login():
    global execution_started, errors, result_df
    if errors:
        return redirect(url_for('error'))
    if not (result_df == []):
        return redirect(url_for('results'))
    if execution_started:
        return redirect(url_for('start'))
    import config
    import app_browser
    if config.browser_task is None:
        config.browser_task =  app_browser.create_browser()
    browser = config.browser_task
    browser.get('http://www.pje.jus.br/navegador/')
    return render_template('confirm_login.html')

@app.route("/start/")
def start():
    global execution_started, progress_bar, errors, result_df, result_messages
    if errors:
        return redirect(url_for('error'))
    if not (result_df == []):
        return redirect(url_for('results'))
    if not execution_started:
        execution_started = True
        threading.Thread(target=lambda: check_all_lawsuits(progress_bar,errors,result_df,result_messages),daemon=True).start()
        return render_template('loading.html')
    return render_template('loading.html')
    
@app.route("/error/")
def error():
    global execution_started, errors, result_df
    if errors is None:
        if not(result_df == []):
            return redirect(url_for('results'))
        if not execution_started:
            return redirect(url_for('index'))
        return redirect(url_for('start'))
    return render_template('errors.html',errors=errors)

@app.route("/results/")
def results():
    global execution_started, errors, result_df
    if not execution_started:
        return redirect(url_for('index'))
    if errors:
        return redirect(url_for('error'))
    if result_df == []:
        return redirect(url_for('start'))
    result_df[0].sort_values('mov_date', ascending=False,  inplace=True)
    orderby = request.args.get('orderby')
    if orderby == 'trial':
        result_df[0].sort_values('trial_date', ascending=False,  inplace=True)
    return render_template(
        'results.html',
        table=result_df[0][[
            'CÓDIGO',
            'NOME',
            'MOVIMENTAÇÃO (DATA)',
            'MOVIMENTAÇÃO (NOME)',
            'AUDIÊNCIA (DATA)',
            'AUDIÊNCIA(STATUS)'
        ]].to_html(
            classes='table table-striped',
            header=True,
            index=False,
            na_rep='',
        ),
        result_messages=result_messages)

@app.route("/reset/")
def reset():
    global execution_started, progress_bar, result_df, errors, result_messages
    if execution_started:
        if (result_df == []) and errors == []:
            return redirect(url_for('start'))
        else:
            import config
            browser = config.browser_task
            if None(browser is None):
                browser.quit()
            config.browser_task = None
            execution_started = False
            progress_bar = []
            result_df = []
            errors = []
            result_messages = []
    return redirect(url_for('index'))

@app.route('/progress/')
def progress():
    def get_progress_bar():
        global progress_bar
        while progress_bar[0] <= 100:
            if errors:
                yield "data:" + 'ERROR' + "\n\n"
                break
            yield "data:" + str(progress_bar[0]) + "\n\n"
            time.sleep(.5)
        if errors is None:
            yield "data:" + '100' + "\n\n"
    return Response(get_progress_bar(), mimetype= 'text/event-stream')

@app.route('/download/')
def download():    
    global result_df, errors, execution_started
    if not execution_started:
        return redirect(url_for('index'))
    if result_df == [] and errors  == []:
        return redirect(url_for('start'))
    if errors:
        return redirect(url_for('error'))
    file_name = 'processos.csv'
    result_df[0].to_csv(temporary_dir + file_name, index=False)
    return send_file(temporary_dir + file_name, as_attachment=True,mimetype='text/csv')

def runserver():
    app.run(port=5000,debug=False,use_reloader=False)

threading.Thread(target=runserver,daemon=True).start()