'''
from selenium import webdriver
driver = webdriver.Firefox(executable_path='driver/geckodriver.exe')
driver.get('https://login.novajus.com.br/conta/login')

find_element_by_xpath //tag[@id='']
.send_keys
.click()
'''


import config
from selenium import webdriver
from selenium.common.exceptions import WebDriverException

driver_location = 'driver/'

def check_browser_closed():
    try:
        config.browser_gui.window_handles
    except:return True
    return False

def create_browser():
    return webdriver.Firefox(executable_path=driver_location + 'geckodriver.exe')

if config.browser_gui is None:
    config.browser_gui = create_browser()